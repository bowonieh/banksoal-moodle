@extends('admin/layout/admin')
@section('content')
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-12">
              <table id="DataTable" style="width:100%;" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>NO</th>
                          <th>Soal</th>
                          <th>Tanggal Dibuat</th>
                          <th>Tanggal Diubah</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                            $no = 1;
                            ?>
                      @foreach ($soal as $s)
                          <tr>
                          <td>{{$no++}}</td>
                          <td>{!!$s->soal!!}</td>
                          <td>{{$s->create_date}}</td>
                              <td>{{$s->last_update}}</td>
                          <td><a href="{{base_url()}}banksoal/edit/{{$s->soal_id}}">edit</a></td>
                          </tr>
                      @endforeach
                  </tbody>
              </table>
              
            </div>
          </div>
        </div>
      </section>
</div>


@endsection
<!-- Halaman Footer -->
@section('footer')
<script>
    $('#DataTable').DataTable();
</script>
@endsection
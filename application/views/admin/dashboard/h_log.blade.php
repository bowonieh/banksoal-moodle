@extends('admin/layout/admin')
@section('content')
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Data pada tabel ini berdasarkan pada pencatatan otomatis yang dilakukan oleh sistem</h3>
                  <br>
                  <hr>
                <button class="btn btn-flat btn-danger" id="ClearLog" act-btn="{{base_url()}}admin/log/clear">Bersihkan Log</button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <table id="DataTable" link-act="{{base_url()}}admin/log/getlog" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>NO</th>
                      <th>LOG Detil</th>
                      <th>Tanggal</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                  
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
        </div>
      </section>
</div>

@endsection
<!-- Halaman Footer -->
@section('footer')
<script>
$('#DataTable').DataTable({
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": $('#DataTable').attr('link-act'),
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
});


$('#ClearLog').on('click',function(a){
  a.preventDefault();
  $.ajax({
    type    : 'POST',
    url     : $('#ClearLog').attr('act-btn'),
    success : function(data){
      var resp = $.parseJSON(data);
      if(resp.status == 'success'){
        
                //window.location.href = "dashboard";
                window.location.reload();
            
      }else{
        
      }
    }
  });
});
</script>

@endsection
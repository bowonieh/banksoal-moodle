@extends('admin/layout/admin')
@section('content')
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/dashboard">Home</a></li>
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/profil">Profil</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-12">
                <div class="col-md-12">
                    <div class="card card-secondary">
                      <div class="card-header">
                        <h3 class="card-title">Form {{$title}}</h3>
          
                        <div class="card-tools">
                          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                            <i class="fas fa-minus"></i></button>
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="form-group">
                          <label for="PasswordLama">Password Lama</label>
                          <input type="password" id="PasswordLama" class="form-control">
                        </div>
                        <div class="form-group">
                          <label for="PasswordBaru">Password Baru</label>
                          <input type="password" id="PasswordBaru" class="form-control" >
                        </div>
                        <div class="form-group">
                          <label for="ConfirmPasswordBaru">Konfirmasi Password</label>
                          <input type="password" id="ConfirmPasswordBaru" class="form-control" >
                        </div>
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" id="ubahPass" class="btn btn-flat btn-primary">Ubah Password</button>
                        <button class="btn btn-flat btn-danger float-right">Cancel</button>
                      </div>
                    </div>
                </div>
                
            </div>
            
          </div>
        </div>
      </section>
</div>


@endsection
<!-- Halaman Footer -->
@section('footer')

@endsection

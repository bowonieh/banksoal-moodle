@extends('admin/layout/admin')
@section('content')
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
           
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              
              <div class="card">
                <div class="card-header">
                
               
                <h3>{{$title}}</h3>
                    {{-- @if(!empty($dtEdit->logo))
                        <img id="logoBrand" class="img-size-50 mr-3 img-circle" src="{{base_url()}}assets/upload/img/brandlogo/{{$dtEdit->logo}}" />
                    @else 
                        <img id="logoBrand" class="img-size-50 mr-3 img-circle" src="{{base_url()}}assets/upload/img/no_logo.png" />
                    @endif --}}
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form enctype="multipart/form-data" id="tambahfield" act-link="{{ base_url()}}admin/promo/aksitambah">
                        <div class="form-group">
                            <label for="judul_promo">Judul Promo</label>
                            <input type="text" class="form-control" id="judul_promo" name="judul_promo" required/>
                            <input type="hidden" id="id_promo" name="id_promo"/>
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="gambar" name="gambar" required/>
                                <label class="custom-file-label" for="gambar"></label>
                              </div>
						  							</div>
						  		  
						  							<div class="form-group">
                            <label for="tanggal mulai">Tanggal Mulai Promo</label>
                            <div class="input-type-prepend">
                                <input type="date" class="form-control" id="tgl_mulaipromo" name="tgl_mulaipromo" required/>
                              </div>
						  							</div>
						  
						  							<div class="form-group">
                            <label for="tanggal selesai">Tanggal Selesai Promo</label>
                            <div class="input-type-prepend">
                                <input type="date" class="form-control" id="tgl_selesaipromo" name="tgl_selesaipromo" required/>
                              </div>
                          </div>


                        <div class="form-group">
                          <label for="deskripsi">Deskripsi</label>
                          <textarea class="form-control" id="detil_promo" name="detil_promo" rows="3" required></textarea>
                        </div>
                        <div class="form-group row">
                            <div>
                              <button type="submit" id="submit" class="btn btn-info">Simpan</button>
                            </div>
                          </div>   
                
                    </form>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
        </div>
      </section>
      <div id="toastElement"></div>
</div>
@endsection
@section('footer')
    <script>
        $('#tambahfield').on('submit',function(e){
            e.preventDefault();
            var file_data = $('#gambar').prop('files')[0];
            var form_data = new FormData(this);
            //form_data.append('logo', file_data);
            //console.log($('#tambahfield').attr('act-link'));
            $.ajax({
             url    :   $('#tambahfield').attr('act-link'),
             type   :   'POST',
             data   :  form_data,
             cache: false,
             contentType: false,
             processData: false,
             success: function(s){
                var resp = $.parseJSON(s);
                if(resp.status == 'success'){
									Swal.fire({
                icon: 'success',
                title: 'Mohon Tunggu',
                text: resp.pesan,
                footer: ''
              }).then(function(){
                window.location.href = "<?php echo base_url('admin/promo'); ?>";
              });
                }else{
									Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: resp.pesan,
                footer: ''
              });
							window.location.href = "<?php echo base_url('admin/promo'); ?>";
            }
           }
         });
        });

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
@endsection

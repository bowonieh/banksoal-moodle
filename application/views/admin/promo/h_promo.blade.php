@extends('admin/layout/admin')
@section('content')
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{$title}}</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Data Promo</h3>
                  <br>
                  <hr>
                <a href="{{base_url()}}admin/promo/tambah"><button class="btn btn-flat btn-primary" >Tambah Promo</button></a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                <table id="DataTable" link-act="{{base_url()}}admin/promo/getpromo" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No</th>
											<th>Judul</th>
											<th>Gambar</th>
											<th>Tanggal Mulai Promo</th>
											<th>Tanggal Berakhir Promo</th>
                      <th>Aksi</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                     
                    </tbody>
                  
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
        </div>
      </section>
</div>

@endsection
<!-- Halaman Footer -->
@section('footer')
<script>
var myDataTable=$('#DataTable').DataTable({
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": $('#DataTable').attr('link-act'),
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ]
});

$(document).on('click', '#disableMerk', function(){ 
        //var url = $('#disableMerk').attr('act-btn');
        //var data = myDataTable.row( $(this).closest('td') ).data();
        var data = $(this).closest('button#disableMerk').attr('id_promo');
        var url = $(this).closest('button#disableMerk').attr('act-btn');
        //var id = $('#disableMerk').attr('id_promo');
        //console.log(url);
        $.ajax({
        type    : 'POST',
        data    : {
            'id_promo'      :data
            },
        url     : url,
        success : function(data){
            var resp = $.parseJSON(data);
            if(resp.status == 'success'){
        
                //window.location.href = "dashboard";
                //window.location.reload();
                myDataTable.ajax.reload();
                
            
                    }else{
        
                }
            }
        });
});

$(document).on('click', '#enableMerk', function(){ 
        //var url = $('#disableMerk').attr('act-btn');
        //var data = myDataTable.row( $(this).closest('td') ).data();
        var data = $(this).closest('button#enableMerk').attr('id_promo');
        var url = $(this).closest('button#enableMerk').attr('act-btn');
        //var id = $('#disableMerk').attr('id_promo');
        //console.log(url);
        $.ajax({
        type    : 'POST',
        data    : {
            'id_promo'      :data
            },
        url     : url,
        success : function(data){
            var resp = $.parseJSON(data);
            if(resp.status == 'success'){
        
                //window.location.href = "dashboard";
                //window.location.reload();
                //myDataTable.ajax.reload();
                //
                $('button#enableMerk').attr('act-btn','promo/disable');
                $('button#enableMerk').removeClass('btn-info');
                $('button#enableMerk').addClass('btn-danger');
                $('button#enableMerk').text('Disable');
                $('button#enableMerk').attr('id','disableMerk');

            
                    }else{
        
                }
            }
        });
});

</script>

@endsection

@extends('admin/layout/admin')
@section('content')
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
           
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url()}}admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active">{{$title}}</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12">
              
              <div class="card">
                <div class="card-header">
                
               
                <h3>{{$title}}</h3>
                    @if(!empty($dtEdit->gambar))
                        <img id="logoBrand" class="img-size-50 mr-3 img-circle" src="{{base_url()}}assets/upload/img/brandlogo/{{$dtEdit->logo}}" />
                    @else 
                        <img id="logoBrand" class="img-size-50 mr-3 img-circle" src="{{base_url()}}assets/upload/img/no_logo.png" />
                    @endif
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                        <div class="form-group">
                            <label for="nama_merk">Nama Promo</label>
                            <input type="text" class="form-control" id="nama_merk" name="nama_merk" value="{{$dtEdit->judul_promo}}" readonly/>
                            <input type="hidden" id="id_merk" name="id_merk" value="{{$dtEdit->id_promo}}"/>
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Gambar</label>
                            <div class="custom-file">
															@if(!empty($dtEdit->gambar))
															<img id="logoPromo" class="img-size-50 mr-3" src="{{base_url()}}assets/upload/promo/{{$dtEdit->gambar}}" alt="logo {{$dtEdit->gambar}}"/>
															<small>{{$dtEdit->gambar}}</small>
													@else 
															<img id="logoPromo" class="img-size-50 mr-3" src="{{base_url()}}assets/upload/img/no_logo.png" />
															<small>Tidak Ada logo</small>
													@endif
                              </div>
                          </div>
                          
                        <div class="form-group">
                          <label for="deskripsi">Deskripsi</label>
                          <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" readonly>{{$dtEdit->detil_promo}}</textarea>
                        </div>
                        <div class="form-group row">
                            <div>
							<a class="btn btn-primary" href="{{ base_url() }}admin/promo/">Kembali</a>
                            </div>
                          </div>   
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
          </div>
        </div>
      </section>
      <div id="toastElement"></div>
</div>
@endsection
@section('footer')
    <script>
        $('#editField').on('submit',function(e){
            e.preventDefault();
            var file_data = $('#logo').prop('files')[0];
            var form_data = new FormData(this);
            //form_data.append('logo', file_data);
            //console.log($('#editField').attr('act-link'));
            $.ajax({
             url    :   $('#editField').attr('act-link'),
             type   :   'POST',
             data   :  form_data,
             cache: false,
             contentType: false,
             processData: false,
             success: function(s){
                var resp = $.parseJSON(s);
                if(resp.status == 'success'){
                   
                }else{
                   
                }
           }
         });
        });

        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
@endsection

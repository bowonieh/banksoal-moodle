<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo e($title); ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo e(base_url()); ?>admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active"><?php echo e($title); ?></li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-3 col-6">
              
            </div>
          </div>
        </div>
      </section>
</div>


<?php $__env->stopSection(); ?>
<!-- Halaman Footer -->
<?php $__env->startSection('footer'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banksoal\application\views/admin/dashboard/h_dashboard.blade.php ENDPATH**/ ?>
<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo e($title); ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo e(base_url()); ?>admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active"><?php echo e($title); ?></li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-12">
                
                      <span class="anchor" id="formComplex"></span>
                      <hr class="my-5">
                      <!-- form complex example -->
                      <div class="card card-outline-secondary">
                        <div class="card-header">
                          <h3 class="mb-0">Form Edit Soal</h3>
                          <div class="float-right">
                            <button id="addJawabanList" class="btn btn-danger">Tambah Jawaban</button>
                            <input class="btn btn-secondary" type="reset" value="Cancel"> 
                                              <input class="btn btn-primary" type="button" value="Send">
                          </div>
                        </div>
                        <div class="card-body">
                          <div class="row mt-4">
                            <div class="col-6">
                              <div class="col-sm-12">
                                <label for="pertanyaan">Pertanyaan</label> 
                                <textarea class="form-control editor" id="soal" rows="5"><?php echo $soal->soal; ?></textarea> 
                                <small class="text-muted">Add any notes here.</small>
                              </div>
                             
                              <div class="col-sm-12 pb-3">
                                <label for="KompetensiDasar">Kompetensi Dasar</label> 
                                <textarea class="form-control editor" id="kompetensiDasar" rows="5"></textarea> 
                              </div>
                              <div class="col-sm-12 pb-3">
                                <label for="materi">Materi</label> 
                                <textarea class="form-control editor" id="materi" rows="5"></textarea> 
                              </div>
                              <div class="col-sm-12 pb-3">
                                <label for="indikator">Indikator</label> 
                                <textarea class="form-control editor" id="indikator" rows="5"></textarea> 
                              </div>
                            </div>
                            
                            <div class="col-sm-6">
                              <div class="col-sm-12">
                                <h6><strong>Daftar Jawaban</strong></h6>
                                <hr>
                            </div>
                            <div class="col-md-12">
                              <div id="jawaban-list">
                                <div class="card" >
                                  <!--CARD HEADER-->
                                  <h4 class="card-header">HEADER <small class="float-sm-right">right aligned</small></h4>
                                  <!--CARD BODY-->
                                  <div class="card-body">
                                  </div>
                                </div>
                                
                              </div>
                            </div>
                            </div>
                          </div>
                        </div>
                        <div class="card-footer">
                          <!--<div class="float-right">
                            <button id="addJawabanList" class="btn btn-danger">Tambah Jawaban</button>
                            <input class="btn btn-secondary" type="reset" value="Cancel"> 
                                              <input class="btn btn-primary" type="button" value="Send">
                          </div>-->
                        </div>
                      </div><!--/card-->
                  
              
            </div>
          </div>
        </div>
      </section>
</div>


<?php $__env->stopSection(); ?>
<!-- Halaman Footer -->
<?php $__env->startSection('footer'); ?>
<script>
    var base_url = '<?php echo base_url();?>';
    $('#DataTable').DataTable();

    //ck_editor
    //Upload
    function uploadImageContent(image, editor) {
          var data = new FormData();
          data.append("image", image);
          $.ajax({
          url: '<?php echo base_url();?>banksoal/upload',
          cache: false,
          contentType: false,
          processData: false,
          data: data,
          type: "post",
          success: function(url) {
            var resp = $.parseJSON(url);
              //var image = $('<img>').attr('src', resp.file_name);
              //$(editor).summernote("insertNode", image[0]);
          },
            error: function(data) {
                console.log(data);
            }
          });
      }
    

            tinymce.init({
										selector: '.editor',
										plugins: [
											'advlist autolink lists link image charmap print preview hr anchor pagebreak',
											'searchreplace wordcount visualblocks visualchars code fullscreen',
											'insertdatetime media nonbreaking save table contextmenu directionality',
											'emoticons template paste textcolor colorpicker textpattern imagetools uploadimage paste formula'
										],
										
										toolbar: 'bold italic fontselect fontsizeselect | alignleft aligncenter alignright bullist numlist  backcolor forecolor | formula code | imagetools link image paste ',
										fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
										paste_data_images: true,
                    images_upload_url: '<?php echo base_url();?>banksoal/upload',
                    convert_urls: false,  
                    //images_upload_base_path: '<?php echo base_url();?>',
										
										/*images_upload_handler: function (blobInfo, success, failure) {
                      
											///success('data:' + blobInfo.blob().type + ';base64,' + blobInfo.base64());
                      
										},*/
                   
										image_class_list: [
										{title: 'Responsive', value: 'img-responsive'}
										],									
										});

          tinymce.init({
            selector: '.editor1',
          });

          //tambah list jawaban
         /* $("#addJawabanList").click(function () {
                $("#jawaban-list").append(
                  '<div class="col-sm-10"><textarea class="form-control answer-list" id="indikator" rows="5" name="jawaban[]"></textarea></div>'
                );
                tinymce.execCommand('mceAddEditor', false, '.answer-list'); 
               });*/
          

       
                      
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banksoal\application\views/admin/banksoal/h_editsoal.blade.php ENDPATH**/ ?>
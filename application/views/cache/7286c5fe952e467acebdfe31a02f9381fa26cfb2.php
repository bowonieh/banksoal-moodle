<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo e($title); ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo e(base_url()); ?>admin/dashboard">Home</a></li>
              <li class="breadcrumb-item active"><?php echo e($title); ?></li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>

      <!-- section -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-12">
              <table id="DataTable" style="width:100%;" class="table table-bordered table-striped">
                  <thead>
                      <tr>
                          <th>NO</th>
                          <th>Soal</th>
                          <th>Tanggal Dibuat</th>
                          <th>Tanggal Diubah</th>
                          <th>Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                            $no = 1;
                            ?>
                      <?php $__currentLoopData = $soal; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $s): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                          <td><?php echo e($no++); ?></td>
                          <td><?php echo $s->soal; ?></td>
                          <td><?php echo e($s->create_date); ?></td>
                              <td><?php echo e($s->last_update); ?></td>
                          <td><a href="<?php echo e(base_url()); ?>banksoal/edit/<?php echo e($s->soal_id); ?>">edit</a></td>
                          </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
              </table>
              
            </div>
          </div>
        </div>
      </section>
</div>


<?php $__env->stopSection(); ?>
<!-- Halaman Footer -->
<?php $__env->startSection('footer'); ?>
<script>
    $('#DataTable').DataTable();
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin/layout/admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\banksoal\application\views/admin/banksoal/h_banksoal.blade.php ENDPATH**/ ?>
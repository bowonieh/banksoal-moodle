<?php

class Auth extends CI_Controller{
    
    function __construct()
    {
        parent::__construct();

    }

    function pwd(){
        echo hash_password('admin');
    }

    function index(){
        //$a = hash_password('test');
        //echo password_verify('test',$a);
        if(is_logged_in()){
            redirect('backend/dashboard','refresh');
        }else{
            //tampilin halaman login
            $data = array(
              'title' =>  'Login banksoal.id'
            );
            return view('admin.login.h_login',$data);

        }

    }

    function checklogin(){
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $h_password = password_hash($password, PASSWORD_BCRYPT);
        $data = array(
            'username'  => $username  
        );

        $this->db->where($data);
        $a = $this->db->get('user');
        if($a->num_rows() > 0){
            //validasi password
            $b = $a->row();
            //echo password_verify($password, $b->password);
            if(password_verify($password,$b->password)){
                //password cocok

                $this->session->set_userdata('user_id',$b->user_id);
                $this->session->set_userdata('level',$b->level);
                $log = array(
                    'log_detil' => 'User '.$b->username.' Berhasil Masuk Ke dalam sistem'
                );

               // $this->db->insert('tbl_log',$log);

                $msg = array(
                    'status'    => 'success',
                    'pesan'     => 'Login Berhasil'
                );
                echo json_encode($msg);
            }else{
                $msg = array(
                    'status'    => 'failed',
                    'pesan'     => 'Password tidak cocok'
                );
                echo json_encode($msg);
                

			}

            
        }else{
			
            $msg = array(
                'status'    => 'failed',
                'pesan'     => 'Username dan Password salah'
            );
            echo json_encode($msg);
		}
		

    }

    function logout(){
        if(!is_logged_in()){
            redirect('auth','refresh');
        }
        
        //eksekusi
        $user_id = $this->session->userdata('user_id');
        
    
        
       
            $this->session->sess_destroy();
            redirect('auth','refresh');
        
        
    }
}

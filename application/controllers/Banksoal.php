<?php

class Banksoal extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        if(!is_logged_in()):
            redirect('auth','refresh');
        endif;
    }
    public function upload(){
		$config['upload_path']          = realpath(FCPATH.'./assets/upload/img/soal');
		$config['allowed_types']        = 'gif|jpg|png';
		
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
 
		$this->load->library('upload', $config);
 
		if ( ! $this->upload->do_upload('file')){
			$error = array('error' => $this->upload->display_errors());
            //$this->load->view('v_upload', $error);
           
            $msg = array(
                'pesan' => 'gagal'
            );
            echo json_encode($msg);
		}else{
			$data = array('upload_data' => $this->upload->data());
            //$this->load->view('v_upload_sukses', $data);
            //var_dump($data);
            //echo $data['upload_data']['file_name'];
            $url = @base_url();
            $msg = array(
                'pesan' => 'success',
                'location' => $url.'assets/upload/img/soal/'.$data['upload_data']['file_name']
            );
            echo json_encode($msg);
		}
    }
    
    function edit($id = null){
        $this->db->where(array('soal_id'=> $id,'user_id'=>pengguna()->user_id));
        $a = $this->db->get('soal');

        //ambil jawaban_soal
        $this->db->where(array('soal_id'=> $id));
        $b = $this->db->get('jawaban_soal');
        if($a->num_rows() > 0):
                    $data = array(
                        'title' => 'Bank Soal - edit soal',
                        'pengguna'  => pengguna(),
                        'apltitle' => $this->config->item('apltitle'),
                        'soal'      => $a->row(),
                        'jawaban_soal'  => $b->result()
                    );   
                    //echo json_encode($data); 
                    return view('admin.banksoal.h_editsoal',$data);
        else:
                    redirect('banksoal','refresh');      
        endif;
        
    }
    function index(){
        
        if(checklevel() === '1'):
            //echo "admin";
            $a = $this->db->get('soal');
         
        elseif(checklevel() === '99'):
            //echo "Guru";
            $this->db->where(array('user_id'=>pengguna()->user_id));
            $a = $this->db->get('soal');
          
            
        endif;
        $data = array(
            'title' => 'Bank Soal',
            'pengguna'  => pengguna(),
            'apltitle' => $this->config->item('apltitle'),
            'soal'      => $a->result()
        );
        return view('admin.banksoal.h_banksoal',$data);
        //var_dump($data);
        
        

    }
}
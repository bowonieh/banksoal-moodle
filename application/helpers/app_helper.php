<?php
function RandImg($dir)
{
$images = glob($dir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);

$randomImage = $images[array_rand($images)];
return $randomImage;
}

function hash_password($pass_user) {
    return password_hash($pass_user, PASSWORD_BCRYPT);
   }

function ambilsetting($key = null){
    $this->load->model('settingmodel');
    //$a = Settingmodel::where($key)->get();
    //return $a;
}
   
   
function is_logged_in(){
    $CI =& get_instance();

    $user = $CI->session->userdata('user_id');
    if(!isset($user)){
        return false;
    }else{
        return true;
    }   
}
function checklevel(){
    $CI =& get_instance();
    $level = $CI->session->userdata('level');
    
    return $level;
}

function pengguna(){
    $CI =& get_instance();

    $user_id = $CI->session->userdata('user_id');


    $CI->db->where(array('u.user_id' => $user_id));
    
    //$CI->db->join('tbl_profil p','p.user_id = u.user_id','left');
    //$CI->db->join('ref_provinsi rp','rp.id_provinsi = p.id_provinsi','left');
    $a = $CI->db->get('user u');

    return $a->row();
}

function hitung_umur($tanggal_lahir) {
    list($year,$month,$day) = explode("-",$tanggal_lahir);
    $year_diff  = date("Y") - $year;
    $month_diff = date("m") - $month;
    $day_diff   = date("d") - $day;
    if ($month_diff < 0) $year_diff--;
        elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
    return $year_diff;
}

function hitungUsia($d = null){
    $birthDt = new DateTime($d);
    //tanggal hari ini
    $today = new DateTime('today');
    //tahun
    $y = $today->diff($birthDt)->y;
    //bulan
    $m = $today->diff($birthDt)->m;
    //hari
    $d = $today->diff($birthDt)->d;
    return  $y . " tahun " . $m . " bulan " . $d . " hari";
}

function jenkel($j = null){
    if($j === 'L'){
        return "Laki-Laki";
    }else{
        return "Perempuan";
    }

}

function firstImg($html = null){
   
    $doc = new DOMDocument();
    libxml_use_internal_errors(true);
    $doc->loadHTML($html); // loads your html
    $xpath = new DOMXPath($doc);
    $nodelist = $xpath->query("//img"); // find your image
    $node = $nodelist->item(0); // gets the 1st image
    //echo $node;
    if(empty($node)){
        
        return 'assets/img_upload/default.png';
    }else{
        $value = $node->attributes->getNamedItem('src')->nodeValue;
        return $value;
    }
   
    //echo "src=$value\n"; // prints src of image
}
function rupiah($angka){
	
    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
    return $hasil_rupiah;
 
}